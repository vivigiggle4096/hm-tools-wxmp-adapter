module.exports = {
	title : 'JsCss',
	description : 'hm-tools-js-css技术文档',
	port : 8082,
	head : [
	// 增加一个自定义的 favicon(网页标签的图标)
	// [ 'link', {
	// rel : 'icon',
	// href : '/img/sbu_favicon.ico'
	// } ],
	[
			'meta',
			{
				name : 'keywords',
				content : 'JavaScript,css,静态资源,合并,Java,hm-tools,jscss,spring-boot-starter-js-css'
			} ] ],
	themeConfig : {
		nav : [ {
			text : '首页',
			link : '/'
		}, {
			text : '指南',
			link : '/guide/'
		}, {
			text : '配置',
			link : '/config/'
		}, {
			text : 'gitee',
			link : 'https://gitee.com/hm-tools/hm-tools-js-css'
		}, {
			text : 'CSDN',
			link : 'https://blog.csdn.net/hybomyth'
		}, {
			text : '个人网站',
			link : 'http://www.hybomyth.com'
		} ],
		smoothScroll : true,
		sidebar : {
			'/guide/' : ['','1.项目引用','2.独立部署'],
			'/config/' : ['']
//			'/guide/' : genGuideSideBar(),
//			'/config/' : genConfigSideBar()
		},
		displayAllHeaders : true,
		sidebarDepth : 2
	},
	plugins : [ '@vuepress/back-to-top', '@vuepress/blog' ]
}

/**
 * 生成指南
 * 
 * @returns {Array}
 */
function genGuideSideBar() {
	return [ {
		title : "开始",
		collapsable : false,
		children : [ "", "1", "2" ]
	}, {
		title : "试验",
		collapsable : false,
		children : [ "", "1", "2" ]
	}, ]
}
