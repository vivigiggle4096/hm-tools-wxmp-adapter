package top.hmtools.wxmp.core.configuration;

import java.io.Serializable;

/**
 * 微信公众号平台appid与appsecret数据信息对
 * @author HyboWork
 *
 */
public class AppIdSecretPair implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2823846409844333799L;

	/**
	 * 在微信注册的微信公众号应用主键id
	 */
	private String appid;

	/**
	 * 在微信注册的微信公众号应用秘钥
	 */
	private String appsecret;

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppsecret() {
		return appsecret;
	}

	public void setAppsecret(String appsecret) {
		this.appsecret = appsecret;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AppIdSecretPair [appid=" + appid + ", appsecret=" + appsecret + "]";
	}
	
	
}
