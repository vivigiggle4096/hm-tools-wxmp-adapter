package top.hmtools.javabean.models;

import java.io.File;
import java.io.InputStream;

public class BbbBean {

	private String bbbName;
	
	private InputStream inputStream;
	
	private File file;
	
	private CccBean cccBean;

	public String getBbbName() {
		return bbbName;
	}

	public void setBbbName(String bbbName) {
		this.bbbName = bbbName;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public CccBean getCccBean() {
		return cccBean;
	}

	public void setCccBean(CccBean cccBean) {
		this.cccBean = cccBean;
	}

	@Override
	public String toString() {
		return "BbbBean [bbbName=" + bbbName + ", inputStream=" + inputStream + ", file=" + file + ", cccBean="
				+ cccBean + "]";
	}
	
	
}
