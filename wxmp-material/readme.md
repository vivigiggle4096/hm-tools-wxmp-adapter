[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/wxmp-material.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22wxmp-material%22)
#### 前言
本组件对应实现微信公众平台“素材管理”章节相关api接口，原接口文档地址：[素材管理](https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/New_temporary_materials.html)

#### 接口说明
- `top.hmtools.wxmp.material.apis.ITemporaryApi` 对应临时素材相关接口
- `top.hmtools.wxmp.material.apis.IForeverApi` 	对应永久素材相关接口

#### 事件消息类说明
`略`

#### 使用示例
0. 引用jar包
```
<dependency>
  <groupId>top.hmtools</groupId>
  <artifactId>wxmp-material</artifactId>
  <version>1.0.0</version>
</dependency>
```

1. 获取wxmpSession，参照 [wxmp-core/readme.md](../wxmp-core/readme.md)
```
//2 获取动态代理对象实例
ITemporaryApi temporaryApi = this.wxmpSession.getMapper(ITemporaryApi.class);

//3 获取自定义菜单数据并打印
File file = new File("E:\\tmp\\aaaa.png");
if(!file.exists()){
	System.out.println(file+"文件不存在");
	return;
}

UploadParam uploadParam = new UploadParam();
uploadParam.setMedia(file);
uploadParam.setType(MediaType.image);
MediaBean mediaBean = this.temporaryApi.uploadMedia(uploadParam);
```

更多示例参见：
- [临时素材](src/test/java/top/hmtools/wxmp/material/apis/ITemporaryApiTest.java)
- [永久素材](src/test/java/top/hmtools/wxmp/material/apis/IForeverApiTest.java)

2020.06.02	 素材组件全部测试OK