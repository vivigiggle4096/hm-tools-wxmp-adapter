package top.hmtools.wxmp.menu.models.simple;

/**
 * 图文消息的信息
 * @author Hybomyth
 *
 */
public class NewsInfoBean {

	/**
	 * 作者
	 */
	private String author;
	
	/**
	 * 正文的URL
	 */
	private String content_url;
	
	/**
	 * 封面图片的URL
	 */
	private String cover_url;
	
	/**
	 * 摘要
	 */
	private String digest;
	
	/**
	 * 是否显示封面，0为不显示，1为显示
	 */
	private String show_cover;
	
	/**
	 * 原文的URL，若置空则无查看原文入口
	 */
	private String source_url;
	
	/**
	 * 图文消息的标题
	 */
	private String title;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent_url() {
		return content_url;
	}

	public void setContent_url(String content_url) {
		this.content_url = content_url;
	}

	public String getCover_url() {
		return cover_url;
	}

	public void setCover_url(String cover_url) {
		this.cover_url = cover_url;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getShow_cover() {
		return show_cover;
	}

	public void setShow_cover(String show_cover) {
		this.show_cover = show_cover;
	}

	public String getSource_url() {
		return source_url;
	}

	public void setSource_url(String source_url) {
		this.source_url = source_url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "NewsInfoBean [author=" + author + ", content_url=" + content_url + ", cover_url=" + cover_url
				+ ", digest=" + digest + ", show_cover=" + show_cover + ", source_url=" + source_url + ", title="
				+ title + "]";
	}
	
	
}
