package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理 之 客服消息 之 图片消息
 * @author hybo
 *
 */
public class ImageCustomerServiceMessage extends BaseSendMessageParam {

	/**
	 * 图片消息
	 */
	private MediaBean image;

	public MediaBean getImage() {
		return image;
	}

	public void setImage(MediaBean image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "ImageCustomerServiceMessage [image=" + image + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}
	
	
}
