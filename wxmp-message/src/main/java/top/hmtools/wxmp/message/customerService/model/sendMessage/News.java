package top.hmtools.wxmp.message.customerService.model.sendMessage;
import java.util.List;

/**
 * Auto-generated: 2019-08-25 15:45:8
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class News {

    private List<Articles> articles;
    public void setArticles(List<Articles> articles) {
         this.articles = articles;
     }
     public List<Articles> getArticles() {
         return articles;
     }

}