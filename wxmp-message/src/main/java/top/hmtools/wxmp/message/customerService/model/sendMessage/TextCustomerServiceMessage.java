package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 客服接口-发消息-文本消息
 * @author hybo
 *
 */
public class TextCustomerServiceMessage extends BaseSendMessageParam {

	/**
	 * 文本内容实体类
	 */
	private TextBean text;

	public TextBean getText() {
		return text;
	}

	public void setText(TextBean text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "TextCustomerServiceMessage [text=" + text + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}
	
	
}
