package top.hmtools.wxmp.message.group.model.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "item",impl=ItemType.class)
public class ItemType {

	@XStreamAlias(value = "ArticleIdx")
	protected Integer articleIdx;
	
	@XStreamAlias(value = "UserDeclareState")
	protected Integer userDeclareState;
	
	@XStreamAlias(value = "AuditState")
	protected Integer auditState;
	
	@XStreamAlias(value = "OriginalArticleUrl")
	protected String originalArticleUrl;
	
	@XStreamAlias(value = "OriginalArticleType")
	protected Integer originalArticleType;
	
	@XStreamAlias(value = "CanReprint")
	protected Integer canReprint;
	
	@XStreamAlias(value = "NeedReplaceContent")
	protected Integer needReplaceContent;
	
	@XStreamAlias(value = "NeedShowReprintSource")
	protected Integer needShowReprintSource;

	

	public Integer getArticleIdx() {
		return articleIdx;
	}

	public void setArticleIdx(Integer articleIdx) {
		this.articleIdx = articleIdx;
	}


	public Integer getUserDeclareState() {
		return userDeclareState;
	}

	public void setUserDeclareState(Integer userDeclareState) {
		this.userDeclareState = userDeclareState;
	}

	public Integer getAuditState() {
		return auditState;
	}

	public void setAuditState(Integer auditState) {
		this.auditState = auditState;
	}

	public String getOriginalArticleUrl() {
		return originalArticleUrl;
	}

	public void setOriginalArticleUrl(String originalArticleUrl) {
		this.originalArticleUrl = originalArticleUrl;
	}

	public Integer getOriginalArticleType() {
		return originalArticleType;
	}

	public void setOriginalArticleType(Integer originalArticleType) {
		this.originalArticleType = originalArticleType;
	}

	public Integer getCanReprint() {
		return canReprint;
	}

	public void setCanReprint(Integer canReprint) {
		this.canReprint = canReprint;
	}

	public Integer getNeedReplaceContent() {
		return needReplaceContent;
	}

	public void setNeedReplaceContent(Integer needReplaceContent) {
		this.needReplaceContent = needReplaceContent;
	}

	public Integer getNeedShowReprintSource() {
		return needShowReprintSource;
	}

	public void setNeedShowReprintSource(Integer needShowReprintSource) {
		this.needShowReprintSource = needShowReprintSource;
	}

	@Override
	public String toString() {
		return "ItemType [articleIdx=" + articleIdx + ", userDeclareState=" + userDeclareState + ", auditState="
				+ auditState + ", originalArticleUrl=" + originalArticleUrl + ", originalArticleType="
				+ originalArticleType + ", canReprint=" + canReprint + ", needReplaceContent=" + needReplaceContent
				+ ", needShowReprintSource=" + needShowReprintSource + "]";
	}

}
