package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * 根据标签进行群发用基本消息参数实体类
 * @author HyboWork
 *
 */
public class BaseTagGroupSendParam {
	protected Filter filter;
	protected TagMsgType msgtype;

	public Filter getFilter() {
		return filter;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
	}

	public TagMsgType getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(TagMsgType msgtype) {
		this.msgtype = msgtype;
	}

	@Override
	public String toString() {
		return "BaseTagGroupSendParam [filter=" + filter + ", msgtype=" + msgtype + "]";
	}
}
