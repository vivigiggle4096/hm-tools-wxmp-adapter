package top.hmtools.wxmp.user.model;

import java.util.List;

public class BatchBlackListParam {

	private List<String> openid_list;

	public List<String> getOpenid_list() {
		return openid_list;
	}

	public void setOpenid_list(List<String> openid_list) {
		this.openid_list = openid_list;
	}
	
	
}
