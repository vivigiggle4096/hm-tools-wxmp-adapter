package top.hmtools.wxmp.webpage.models;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class TicketResult extends ErrcodeBean {

	private String ticket;
	
	private int expires_in;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}

	@Override
	public String toString() {
		return "TicketResult [ticket=" + ticket + ", expires_in=" + expires_in + ", errcode=" + errcode + ", errmsg="
				+ errmsg + "]";
	}
	
	
}
